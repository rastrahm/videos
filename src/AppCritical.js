import React from 'react';
import { Button, Alert, Form, FormGroup, Label, Input  } from 'reactstrap';
import SkyLight from 'react-skylight';

export default class AppCritits extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			id: this.props.id,
			critis: this.props.critics,
			login: this.props.login,
			saveCritical: this.props.saveCritical,
			deleteCritical: this.props.deleteCritical,
			words: "",
			calcs: ""
		};
		this.handleChangeCalc = this.handleChangeCalc.bind(this);
		this.handleChangeWord = this.handleChangeWord.bind(this);
    }
	
    componentDidMount() {
		this.setState({ link: this.props.link });
	}
	
	returnComment(values) {
		const list = values.content.map(text => {
			return (
				<tr key={text.id}><td>{text.calificacion}</td><td><b>{text.mensaje}</b></td><td>{values.login() ? (<Button style={{width: 100, height: 30, display: 'inline-block'}} color="danger" size="sm" onClick={() => {values.deleteCritical(text.id, values.view);}}  block>Borrar</Button>) : ( <div></div> )}</td></tr>
			)
		})
		return list;
	}

	handleChangeCalc(event) {
		this.setState({calcs: event.target.value});
	}

	handleChangeWord(event) {
		this.setState({words: event.target.value});
	}

    render() {
		var alertDialog = {
			width: '70%',
			height: '100px',
			'min-height': '50px',
			marginTop: '-300px',
			marginLeft: '-35%',
		};

		var formDialog = {
			width: '70%',
			height: '300px',
			'min-height': '50px',
			marginTop: '-300px',
			marginLeft: '-35%',
		};

		return <div key={this.state.id}>
			<h3>Criticas</h3>
			{
				this.state.login() ? (
					<Button style={{display: 'inline-block'}} onClick={() => this.simpleDialog.show()} color="primary">Añade tu Critica</Button>
				) : (
					<div></div>
				)
			}
			<table style={{width: '100%'}}>
				<this.returnComment content={this.state.critis} login={this.state.login} deleteCritical={this.state.deleteCritical} view={this.state.id}/>
			</table>
			{
				this.state.login() ? (
					<SkyLight dialogStyles={formDialog} hideOnOverlayClicked ref={ref => this.simpleDialog = ref} >
						<Form>
							<FormGroup>
								<Label for="calift">Calificación</Label>
								<Input type="select" name="calift" id="calift" value={this.state.calcs} onChange={this.handleChangeCalc}>
									<option>10</option>
									<option>9</option>
									<option>8</option>
									<option>7</option>
									<option>6</option>
									<option>5</option>
									<option>4</option>
									<option>3</option>
									<option>2</option>
									<option>1</option>
								</Input>
							</FormGroup>
							<FormGroup>
								<Label for="comment">Añade tu critica</Label>
								<Input type="textarea" name="critict" id="critict"  value={this.state.words}  onChange={this.handleChangeWord}/>
							</FormGroup>
							<Button color="primary" onClick={() => {this.state.saveCritical(this.state); this.simpleDialog.hide();}} block>Guardar</Button>
						</Form>
					</SkyLight>
				) : (
					<div>
						<Alert color="primary">
							Si deseas añadir criticas por favor ingresa
						</Alert>
						<SkyLight dialogStyles={alertDialog} hideOnOverlayClicked ref={ref => this.simpleDialog = ref} >
							<div style={{margin: 15}} >
								<Alert color="primary">
									Por favor loggeate
								</Alert>
							</div>
						</SkyLight>
					</div>
				)
			}
			
			</div>
	}
}

