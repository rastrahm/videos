import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

const MenuSize = (props) => {
	const [dropdownOpen, setDropdownOpen] = useState(false);

	const toggle = () => setDropdownOpen(prevState => !prevState);

	return (
		<Dropdown isOpen={dropdownOpen} toggle={toggle}>
			<DropdownToggle caret>
				Peliculas a ver
			</DropdownToggle>
			<DropdownMenu>
				<DropdownItem onClick={() => props.props(5)}>5 Peliculas</DropdownItem>
				<DropdownItem onClick={() => props.props(10)}>10 Peliculas</DropdownItem>
				<DropdownItem onClick={() => props.props(20)}>20 Peliculas</DropdownItem>
				<DropdownItem onClick={() => props.props(50)}>50 Peliculas</DropdownItem>
			</DropdownMenu>
		</Dropdown>
	);
}

export default MenuSize;
//<DropdownItem onClick={() =>  changueSizeView(5)}>5 Peliculas</DropdownItem>