import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

const MenuPager = (props) => {
	let pages = Array(1);
	let init = 0;
	let size = props.size;
	let step = 0;
	if (props !== undefined) {
		pages = Array(props.props);
		step = Math.ceil(size / (size / props.step));
		for (let i = 1; i <= props.props; i++) {
			pages[i] = i;
		}
	} else {
		pages = Array(1);
		pages[1] = 1;
	}

	return (
		<div style={{paddingTop: 15, paddingLeft: 10}}>
			<Pagination aria-label="Paginador">
				<PaginationItem disabled>
					<PaginationLink first href="#" />
				</PaginationItem>
				<PaginationItem disabled>
					<PaginationLink previous href="#" />
				</PaginationItem>
					{pages.map(page => {
						let ant = init;
						init += step;
						if (init > size) {
							init = size;
						}
						return (
							<PaginationItem key={page}>
								<PaginationLink onClick={e => props.func(ant)} href="#">
									{page}
								</PaginationLink>
							</PaginationItem>
						)})
					}
				<PaginationItem>
					<PaginationLink next href="#" />
				</PaginationItem>
				<PaginationItem>
					<PaginationLink last href="#" />
				</PaginationItem>
			</Pagination>
		</div>
	);
}

export default MenuPager;
