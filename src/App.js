import React from 'react';
import { Fragment } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
	NavLink,
	Container,
	Row,
	Col,
	CardColumns
} from 'reactstrap';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect
} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppVideo from './AppVideo';
import AppLogin from './AppLogin';
import AppLink from './AppLink';
import MenuSize from './MenuSize';
import MenuPager from './MenuPager';

export default class App extends React.Component {

	static api = "https://movies.z4.tdplab.com"

	
	constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			isVisible: true,
			options: [],
			link: "",
			data: "",
			sizeView: 10,
			size: 0,
			pages: 0,
			initPag: 0,
			finaPag: 9
		};
		this.api = "https://movies.z4.tdplab.com";
		this.route = "";
		this.enterSystem = this.enterSystem.bind(this);
		this.changeSize = this.changeSize.bind(this);
		this.turnPage = this.turnPage.bind(this);
		this.isLogin = this.isLogin.bind(this);
		this.saveComment = this.saveComment.bind(this);
		this.saveCritical = this.saveCritical.bind(this);
		this.deleteComment = this.deleteComment.bind(this);
		this.deleteCritical = this.deleteCritical.bind(this);
		//const script = document.createElement1("script");
		//script.src = "http://localhost:8097";
		//script.async = true;
		//document.body.appendChild(script);
	}
	
	toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
	}
	
	//Carga los elementos basicos del menú
	async componentDidMount() {
		const response = await fetch(this.api + "/api/pelicula/");
		const json = await response.json();
		for (let i = 0; i < json.length; i++) {
			const comments = await fetch(this.api + "/api/pelicula/" + json[i].id + "/comentarios/");
			const jsonComment = await comments.json();
			json[i].comment = jsonComment;
			const criticas = await fetch(this.api + "/api/pelicula/" + json[i].id + "/criticas/");
			const jsonCriticas = await criticas.json();
			json[i].critics = jsonCriticas;
		}
		this.setState({
			options: json,
			pages: Math.ceil(json.length / this.state.sizeView),
			initPag: 0,
			finaPag: Math.ceil(json.length / (json.length / this.state.sizeView)) - 1,
			size: json.length
		});
	}
	
	async verifyToken() {
		let message = {
			"token": this.state.access
		}
		let response = await fetch(this.api + "/api/token/verify/", {
			method: "POST",
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json",
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		});
		if ((response.status === 200) || (response.status === 201)) {
			return true;
		} else {
			message = {
				"refresh": this.state.refresh
			}	
			response = await fetch(this.api + "/api/token/refresh/", {
				method: "POST",
				mode: "cors", 
				cache: "no-cache", 
				credentials: "same-origin", 
				headers: {
					"Content-Type": "application/json",
				},
				redirect: "follow", 
				referrerPolicy: "no-referrer",
				body: JSON.stringify(message)
			}).then(res => res.json());
			this.setState({
				access: response.access
			});
		}
	}

	saveComment(value) {
		if (value.calcs === "") {
			value.calcs = 10;
		}
		let message = {
			"calificacion": value.calcs,
			"mensaje": value.words,
			"pelicula": value.id
		}
		if (this.verifyToken()) {
			fetch(this.api + "/api/pelicula/" + value.id + "/comentarios/", {
				method: "POST",
				mode: "cors", 
				cache: "no-cache", 
				credentials: "same-origin", 
				headers: {
					"Content-Type": "application/json",
					"Authorization": "Bearer " + this.state.access,
				},
				redirect: "follow", 
				referrerPolicy: "no-referrer",
				body: JSON.stringify(message)
			})
			.then(res => res.json())
			.then(
				(result) => {
					if (result !== "") {
						let arrCopy = this.state.options.slice();
						arrCopy.forEach(function(view, i, values){
							if (view.id === result.pelicula) {
								view.comment.push(result);
								return true;
							}
						});
						this.setState({
							options: arrCopy
						})
					}
			},
			(error) => {
				alert('Usuario invalido');
			})
		}
	}

	saveCritical(values) {
		if (values.calcs === "") {
			values.calcs = 10;
		}
		let value = {
			calificacion: values.calcs,
			id: 0,
			mensaje: values.words,
			pelicula: values.id
		};
		let arrCopy = this.state.options.slice();
		arrCopy.forEach(function(view, i, values){
			if (view.id === value.pelicula) {
				view.critics.push(value);
				return true;
			}
		});
		this.setState({
			options: arrCopy
		})
	}

	deleteComment(id, view) {
		if (this.verifyToken()) {
			let result = fetch(this.api + "/api/pelicula/" + view + "/comentarios/" + id, {
				method: "DELETE",
				mode: "cors", 
				cache: "no-cache", 
				credentials: "same-origin", 
				headers: {
					"Content-Type": "application/json",
					"Authorization": "Bearer " + this.state.access,
				},
				redirect: "follow", 
				referrerPolicy: "no-referrer"
			}).then((res) => {
				if (res.status === 204) {
					let arrCopy = this.state.options.slice();
					arrCopy.forEach((picture) => {
						if (picture.id === view) {
							for (let i = 0; i < picture.comment.length; i++) {
								if (picture.comment[i].id === id) {
									picture.comment.splice(i, 1);
									return true;
								}
							}
						}
					});
					this.setState({
						options: arrCopy
					});
				}
			});
		};
	}

	deleteCritical(id, view) {
		let arrCopy = this.state.options.slice();
		arrCopy.forEach((picture) => {
			if (picture.id === view) {
				for (let i = 0; i < picture.comment.length; i++) {
					if (picture.critics[i].id === id) {
						picture.critics.splice(i, 1);
						return true;
					}
				}
			}
		});
		this.setState({
			options: arrCopy
		});
	}

	turnPage(value) {
		let ends = this.state.finaPag + value;
		this.setState({
			sizeView: this.state.sizeView,
			initPag: value,
			finaPag: ends
		});
	}

	changeSize(value) {
		let init = this.state.initPag;
		let ends = init + value;
		let pages = this.state.pages;
		if (ends > this.state.options.length) {
			ends = this.state.options.length;
		}
		pages = Math.ceil(this.state.options.length / value);
		this.setState({
			sizeView: value,
			initPag: init,
			finaPag: ends,
			pages: pages
		});
	}
	//Guarda el JWT cuando es solicitado
	enterSystem(value) {
		if (value.state.hasOwnProperty("jwt")) {
			this.setState({
				isVisible: false,
				access: value.state.jwt,
				refresh: value.state.refresh
			})
		}
	}

	isLogin() {
		return !this.state.isVisible;
	}
	//Retorna los items de peliculas
	returnCards(values) {
		let end = values.init + values.limit;
		let pre = values.list;
		let pos = Array(0);
		if (values.list.length > 0) {
			if (end > values.list.length) {
				end = values.list.length;
			}
			for (let i= values.init; i < end; i++ ) {
				pos[i] = pre[i];
			}
		}
		const listCards = pos.map((value) => {
			return (
				<Col key={value.id}>
					<AppVideo key={value.id} props={value} route={values.route} login={values.login} access={values.access} saveComment={values.saveComment} saveCritical={values.saveCritical} deleteComment={values.deleteComment} deleteCritical={values.deleteCritical}></AppVideo>
				</Col>
			);
		})
		return listCards;
	}
	
	render() { 
		return <Fragment >
			<Router>
				<Navbar color = "dark" dark expand = "md">
					<NavbarBrand href="/">Peliculas</NavbarBrand>
					<NavbarToggler onClick={this.toggle} />
					<MenuSize props={this.changeSize}></MenuSize>
					<MenuPager props={this.state.pages} func={this.turnPage} size={this.state.size} step={this.state.sizeView}></MenuPager>
				</Navbar>
				<br />
				<Container fluid={true}>
					{
						this.state.isVisible ? (
							<Row>
								<Col sm="9">
										<Row>
										<CardColumns>
											<this.returnCards list={this.state.options} limit={this.state.sizeView} route={this.api} init={this.state.initPag} login={this.isLogin} saveComment={this.saveComment} saveCritical={this.saveCritical} deleteComment={this.deleteComment} deleteCritical={this.deleteCritical}/>
										</CardColumns>
									</Row>
								</Col>
								<Col sm="3">
									<AppLogin enterSystem={this.enterSystem} api={this.api}></AppLogin>
								</Col>
							</Row>
						):(
							<Row>
								<Col sm="12">
									<Row>
										<CardColumns>
											<this.returnCards list={this.state.options} limit={this.state.sizeView} route={this.api} init={this.state.initPag} login={this.isLogin} saveComment={this.saveComment} saveCritical={this.saveCritical} deleteComment={this.deleteComment} deleteCritical={this.deleteCritical}/>
										</CardColumns>
									</Row>
								</Col>
								
							</Row>
						)
					}
				</Container>
			</Router>
		</Fragment>
		
	};
}
