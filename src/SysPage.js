import React from 'react';

export default class SysPage extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			isLink: "",
			data: []
		}
    }

    toggle() {
        this.setState({
			isOpen: !this.state.isOpen,
			isLink: "",
			data: []
		});
    }

    //Carga los elementos basicos del menú
    componentDidMount() {
		if ((this.props.link !== undefined) && (this.props.link !== "")) {
			fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + this.props.link)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.link,
						data: result.data[0]
					});
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		} else {
			this.setState({data: [{str_name: ""}]});
		}
	}
	
	returnPage (){
		if ((this.props.data !== this.props.isLink) && (this.props.data !== undefined) && (this.props.data !== "")) {
			fetch(window.location.protocol + "//" + window.location.hostname + "/funtions/" + this.props.link)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.link,
						data: result.data
					});
					this.render();
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		} else {
			this.setState({data: [{str_name: ""}]});
		}
	}

    render() {
		if ((this.props.data !== this.props.isLink) && (this.props.data !== undefined) && (this.props.data !== "")) {
			//Se paso de this.props.link a this.props.data, una vez verificado el funcionamiento
			//debo crear un componente que muestre el listado de forma adaptable tal como en 
			//menu_program HTML
			return <div>{this.props.data[0].big_id}</div>
		} else {
			return <div></div>
		}
    }
}