import React from 'react';
import SkyLight from 'react-skylight';
import { 
	Button, 
	Form, 
	FormGroup, 
	Label, 
	Input,
	Card,
	CardTitle,
	CardBody,
	CardText
} from 'reactstrap';
import AppComents from './AppComents';
import AppCritits from './AppCritical';

export default class AppVideo extends React.Component {
	constructor(props) {
        super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false,
			isLink: "",
			id: props.props.id,
			key: props.props.id,
			data: props.props,
			api: props.route,
			login: props.login,
			saveComment: props.saveComment,
			saveCritical: props.saveCritical,
			deleteComment: props.deleteComment,
			deleteCritical: props.deleteCritical
		}
    }

    toggle() {
        this.setState({
			isOpen: !this.state.isOpen,
			isLink: "",
			data: []
		});
    }
	

    render() {
		return <div key={this.state.data.id}>	
			<Card body outline color="primary">
				<CardBody className="mb-2 text-muted">
					<CardTitle>
						<h2>{this.state.data.titulo}</h2>
					</CardTitle>
					<CardText>
						<ul>
							{this.state.data.etiquetas.map(value => {
								return (
									<li key={value}>{value}</li>
								);
							})}
						</ul>
					</CardText>
					<Button onClick={() => this.simpleDialog.show()} color="primary">Detalles</Button>
				</CardBody>
			</Card>
			<SkyLight hideOnOverlayClicked ref={ref => this.simpleDialog = ref} title={this.state.data.titulo}>
				<ul>
				{this.state.data.etiquetas.map(value => {
					return (
						<li key={value} className="App-inline"> {value} </li>
					);
				})}
				</ul>
				<AppCritits key={this.state.data.id} id={this.state.id} critics={this.state.data.critics} login={this.state.login} access={this.state.access} saveCritical={this.state.saveCritical} deleteCritical={this.state.deleteCritical}></AppCritits>
				<AppComents key={this.state.data.id + 'C'} id={this.state.id} comment={this.state.data.comment} login={this.state.login} access={this.state.access} saveComment={this.state.saveComment} deleteComment={this.state.deleteComment}></AppComents>
			</SkyLight>
		</div>
    }
}