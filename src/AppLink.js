import React from 'react';
import AppVideo from './AppVideo'

export default class AppLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            link: this.props.link
        };
    }
	
    componentDidMount() {
        this.setState({ link: this.props.link })
	}
	
    render() {
        return <AppVideo link={this.state.link} />
    }
}