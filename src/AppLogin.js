import React from 'react';
import { 
	Button, 
	Form, 
	FormGroup, 
	Label, 
	Input,
	Card,
	CardTitle
} from 'reactstrap';

export default class AppLogin extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			username: "",
			password: ""
		};
		this.handleChangeUser = this.handleChangeUser.bind(this);
		this.handleChangePass = this.handleChangePass.bind(this);
	}

	mySubmitHandler = (event) => {
		event.preventDefault();
		var message = this.state;
		fetch(this.props.api + "/api/token/", {
			method: "POST",
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json"
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
				if (result.access !== "") {
					this.setState({
						jwt: result.access,
						refresh: result.refresh
					});
					this.props.enterSystem(this, false);
				}
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Usuario invalido');
        })
	}

	handleChangeUser(event) {
		this.setState({username: event.target.value});
	}

	handleChangePass(event) {
		this.setState({password: event.target.value});
	}

	render() {
		return (
			<Card body>
				<CardTitle><h3>Acceso al Sistema</h3></CardTitle>
				<Form>
					<FormGroup>
						<Label for="user">Usuario</Label>
						<Input type="text" name="username" id="username" value={this.state.username}  onChange={this.handleChangeUser} placeholder="Introduzca su usuario" />
					</FormGroup>
					<FormGroup>
						<Label for="pass">Password</Label>
						<Input type="password" name="password" id="password" value={this.state.password} onChange={this.handleChangePass} placeholder="Introduzca su password" />
					</FormGroup>
					<Button color="primary" onClick={this.mySubmitHandler} block>Ingresar</Button>
				</Form>
			</Card>
		);
	}
}